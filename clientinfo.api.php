<?php

/**
 * @file
 * clientinfo.api.php
 */

/**
 * Modify the client data.
 *
 * @param array $data
 *   The client information to be altered.
 */
function hook_clientinfo_data_alter(&$data) {
  $data['date'] = date('Y-m-d H:i:s', $data['timestamp']);
}
