# Client Info

This module returns provides network client data.

  * Provides /client-info/json/{context = default}.
  * Hooks are provided to alter the data.
